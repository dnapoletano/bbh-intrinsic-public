c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function delta1(lfr)
c..
c..   Coefficient of the delta-function at NLO.
c..   
c..      implicit real*8 (a-h,o-z)
      implicit none
      double precision lfr
      double precision z2

      z2 = 1.6449340668482264365d0

      delta1 = -1.3333333333333333d0 - 2*lfr + (8*z2)/3.d0

      end


c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function delta2(lfr,lfh)
c..
c..   Coefficient of the delta-function at NNLO.
c..   
      implicit none
      integer nf
      double precision lfr,lfh
      double precision z2,z3,z4

      nf=5

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      delta2 = 6.388888888888889d0 + 3*lfh - (25*lfr)/12.d0 + (19*lfr**2
     &     )/4.d0+(2*nf)/27.d0 + (lfr*nf)/18.d0 - (lfr**2*nf)/6.d0 + (58
     &     *z2)/9.d0 +(8*lfh*z2)/3.d0 - (32*lfh**2*z2)/9.d0 - (38*lfr*z2
     &     )/3.d0 -(10*nf*z2)/27.d0 + (4*lfr*nf*z2)/9.d0 - (26*z3)/3.d0
     &     - (122*lfh*z3)/9.d0 +(2*nf*z3)/3.d0 - (19*z4)/18.d0

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function dterms1(xt,lfh)
c..
c..   The plus-distributions at NLO.
c..   
      implicit none
      double precision xt,lfh,dd0,dd1
      
      dd0 = 1.d0/(1.d0 - xt)
      dd1 = dd0*dlog(1.d0 - xt)

      dterms1 =  (16*dd1)/3.d0 - (8*dd0*lfh)/3.d0

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function dterms2(xt,lfh,lfr)
c..
c..   The plus-distributions at NNLO.
c..   
      implicit none
      integer nf
      double precision xt,lfh,lfr
      double precision dd0,dd1,dd2,dd3
      double precision z2,z3,z4
     

      nf = 5

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      dd0 = 1.d0/(1.d0 - xt)
      dd1 = dd0*dlog(1.d0 - xt)
      dd2 = dd1*dlog(1.d0 - xt)
      dd3 = dd2*dlog(1.d0 - xt)

      dterms2 = (128*dd3)/9.d0 + dd2*(-14.666666666666666d0 - (64*lfh)/3
     &     .d0 +(8*nf)/9.d0) +dd1*(22.666666666666668d0 + (44*lfh)/3.d0
     &     + (64*lfh**2)/9.d0 -(76*lfr)/3.d0 + (-1.4814814814814814d0 -
     &     (8*lfh)/9.d0 + (8*lfr)/9.d0)*nf - (200*z2)/9.d0) +dd0*(-14
     &     .962962962962964d0 - (11*lfh**2)/3.d0 +nf*(0
     &     .691358024691358d0 + (2*lfh**2)/9.d0 +lfh*(0
     &     .7407407407407407d0 - (4*lfr)/9.d0) - (8*z2)/9.d0) +(44*z2)/3
     &     .d0 +lfh*(-11.333333333333334d0 + (38*lfr)/3.d0 +(100*z2)/9
     &     .d0) + (382*z3)/9.d0)

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function dtsub1(lfh,tauh)
c..
c..   Contributions arising from the fact that the integrals over
c..   plus-distributions do not run from 0 to 1, but from z to 1.
c..   
      implicit none
      integer nf
      double precision lfh,tauh
      double precision ddz0,ddz1
      double precision z2,z3,z4

      nf = 5

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      ddz0 = dlog(1.d0 - tauh)
      ddz1 = ddz0**2/2.d0

      dtsub1 =  (16*ddz1)/3.d0 - (8*ddz0*lfh)/3.d0

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function dtsub2(lfh,lfr,tauh)
c..
c..   Contributions arising from the fact that the integrals over
c..   plus-distributions do not run from 0 to 1, but from z to 1.
c..   
      implicit none
      integer nf
      double precision lfh,lfr,tauh
      double precision ddz0,ddz1,ddz2,ddz3
      double precision z2,z3,z4

      
      
      ddz0 = dlog(1.d0 - tauh)
      ddz1 = ddz0**2/2.d0
      ddz2 = ddz0**3/3.d0
      ddz3 = ddz0**4/4.d0

      dtsub2 = (128*ddz3)/9.d0 + ddz2*(-14.666666666666666d0 - (64*lfh)
     &     /3.d0 +(8*nf)/9.d0) +ddz1*(22.666666666666668d0 + (44*lfh)/3
     &     .d0 + (64*lfh**2)/9.d0 -(76*lfr)/3.d0 + (-1
     &     .4814814814814814d0 - (8*lfh)/9.d0 + (8*lfr)/9.d0)*nf - (200
     &     *z2)/9.d0) +ddz0*(-14.962962962962964d0 - (11*lfh**2)/3.d0
     &     +nf*(0.691358024691358d0 + (2*lfh**2)/9.d0 +lfh*(0
     &     .7407407407407407d0 - (4*lfr)/9.d0) - (8*z2)/9.d0) +(44*z2)/3
     &     .d0 +lfh*(-11.333333333333334d0 + (38*lfr)/3.d0 +(100*z2)/9
     &     .d0) + (382*z3)/9.d0)

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function sbbq1(xt,lfh)
C..
C..   sbbq1(xt) is the one-loop result minus the purely soft terms
C..
      implicit none
      double precision xt,lfh
      double precision dlxm1,xm1,dlnx

      dlxm1 = dlog(1-xt)
      xm1 = 1.d0-xt
      dlnx = dlog(xt)

      sbbq1 = (16*dlnx)/3.d0 - (32*dlxm1)/3.d0 + (16*lfh)/3.d0 - (8*dlnx
     &     )/(3.d0*xm1) +(1.3333333333333333d0 - 4*dlnx + 8*dlxm1 - 4
     &     *lfh)*xm1 +(-1.3333333333333333d0 + (4*dlnx)/3.d0 - (8*dlxm1)
     &     /3.d0 + (4*lfh)/3.d0)*xm1**2

      end

c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function sbg1(xt,lfh)
C..
C..   bg contribution at NLO, exact.
C..   
      implicit none
      double precision xt,lfh
      double precision xm1,dlxm1,dlnx

      xm1 = 1.d0-xt
      dlxm1 = dlog(xm1)
      dlnx = dlog(xt)

      sbg1 = -dlnx/4.d0 + dlxm1/2.d0 - lfh/4.d0 + (0.5d0 + (3*dlnx)/4.d0
     &     - (3*dlxm1)/2.d0 + (3*lfh)/4.d0)*xm1 + (-1.375d0 - dlnx + 2
     &     *dlxm1 - lfh)*xm1**2 + (0.875d0 + dlnx/2.d0 - dlxm1 + lfh/2
     &     .d0)*xm1**3

      end
c.......................................................................


C........................Running mass (Marius) ....................
      double precision function runmass(mass0,api0,apif,nf,nloop)
c..
c..   evaluates the running of the MS-bar quark mass
c..   by expanding the equation
c..   
c..   m(mu) = m(mu0) * exp( \int_a0^af dx gammam(x)/x/beta(x) )
c..   
c..   in terms of alpha_s. The results agree with RunDec.m.
c..   
c..   
c..   Input:
c..   ------
c..   mass0  :  m(mu0)
c..   api0   :  alpha_s(mu0)/pi
c..   apif   :  alpha_s(muf)/pi
c..   nf     :  number of flavors
c..   nloop  :  order of calculation (nloop=1..4)
c..
c..   Output:
c..   -------
c..   massout:  m(muf)
c..   
      implicit real*8 (a-h,o-z)
      real*8 mass0,massfun
      real*8 nf
      external massfun
      parameter(accmass=1.d-6)
      data z3/1.2020569031595942853997/,
     &     z5/1.0369277551433699263/,
     &     pi/3.1415926535897932381/

      beta0 = (33 - 2*nf)/12.d0
      beta1 = (102 - (38*nf)/3.d0)/16.d0
      beta2 = (2857/2.d0 - (5033*nf)/18.d0 + (325*nf**2)/54.d0)/64.d0
      beta3 = (149753/6.d0 + (1093*nf**3)/729.d0 + 3564*z3 + nf**2
     &     *(50065/162.d0 + (6472*z3)/81.d0) - nf*(1078361/162.d0 +
     &     (6508*z3)/27.d0))/256.d0
      
      gamma0 = 1.d0
      gamma1 = (67.33333333333333d0 - (20*nf)/9.d0)/16.d0
      gamma2 = (1249.d0 - (140*nf**2)/81.d0 + 2*nf*(-20.59259259259259d0
     &     - 48*z3) +(8*nf*(-46 + 48*z3))/9.d0)/64.d0
      gamma3 = (28413.91975308642d0 + (135680*z3)/27.d0 + nf**3*(-1
     &     .3662551440329218d0 + (64*z3)/27.d0) + nf**2*(21
     &     .57201646090535d0 - (16*Pi**4)/27.d0 + (800*z3)/9.d0) - 8800
     &     *z5 + nf*(-3397.1481481481483d0 + (88*Pi**4)/9.d0 - (34192
     &     *z3)/9.d0 + (18400*z5)/9.d0))/256.d0
      

      bb1 = beta1/beta0
      bb2 = beta2/beta0
      bb3 = beta3/beta0

      cc0 = gamma0/beta0
      cc1 = gamma1/beta0
      cc2 = gamma2/beta0
      cc3 = gamma3/beta0

      cfunc1 = 1.d0
      cfunc2 = cc1 - bb1*cc0
      cfunc3 = 1/2.d0*((cc1-bb1*cc0)**2 + cc2 - bb1*cc1 + bb1**2*cc0 -
     &     bb2*cc0)
      cfunc4 = (1/6*(cc1 - bb1*cc0)**3 + 1/2*(cc1 - bb1*cc0)*(cc2 - bb1
     &     *cc1 + bb1**2*cc0 - bb2*cc0) + 1/3*(cc3 - bb1*cc2 + bb1**2
     &     *cc1 - bb2*cc1 - bb1**3*cc0 + 2*bb1*bb2*cc0 - bb3*cc0))

      if (nloop.lt.4) then
         cfunc4 = 0.d0
         if (nloop.lt.3) then
            cfunc3 = 0.d0
            if (nloop.lt.2) then
               cfunc2 = 0.d0
               if (nloop.lt.1) then
                  cfunc1 = 0.d0
               endif
            endif
         endif
      endif

      cfuncmu0 = cfunc1 + cfunc2*api0 + cfunc3*api0**2 + cfunc4*api0**3
      cfuncmuf = cfunc1 + cfunc2*apif + cfunc3*apif**2 + cfunc4*apif**3

      runmass = mass0*(apif/api0)**cc0*cfuncmuf/cfuncmu0
      end

