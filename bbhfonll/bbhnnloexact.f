c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabbqa(xx,lfh,lfr)

      implicit none

      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog
      
      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabbqa = 28.814814814814813d0 + (74*dli3a)/3.d0 + (2*dli3b)/3
     &     .d0 - (2*dli3c)/3.d0 +(82*dli3d)/3.d0 - dli3f/18.d0 + dli2b
     &     *(0.3333333333333333d0 + dlnx/3.d0) +(1607*dlnx)/54.d0 + (157
     &     *dlnx**2)/9.d0 + (10*dlnx**3)/3.d0 -(403*dlxm1)/9.d0 - (154
     &     *dlnx*dlxm1)/3.d0 -(368*dlnx**2*dlxm1)/9.d0 +(88*dlxm1**2)/3
     &     .d0 + (584*dlnx*dlxm1**2)/9.d0 - (256*dlxm1**3)/9.d0 +dli2a*(
     &     -4.888888888888889d0 - (236*dlnx)/9.d0 + (136*dlxm1)/9.d0
     &     -(88*lfh)/9.d0) + (7.333333333333333d0+ (86*dlnx)/9.d0 -(128
     &     *dlxm1)/9.d0)*lfh**2 + ((-76*dlnx)/3.d0 +(152*dlxm1)/3.d0)
     &     *lfr +lfh*(22.666666666666668d0 + (77*dlnx)/3.d0 +(136*dlnx
     &     **2)/9.d0 -(88*dlxm1)/3.d0 - (536*dlnx*dlxm1)/9.d0 + (128
     &     *dlxm1**2)/3.d0 -(76*lfr)/3.d0 - (200*z2)/9.d0) - (88*z2)/3
     &     .d0 - (416*dlnx*z2)/9.d0 +(400*dlxm1*z2)/9.d0 + ((-142*dli3a)
     &     /9.d0 - (64*dli3d)/9.d0 -(146*dlnx)/9.d0 - (23*dlnx**2)/3.d0
     &     - (44*dlnx**3)/27.d0 +24*dlnx*dlxm1 + (148*dlnx**2*dlxm1)/9
     &     .d0 - (248*dlnx*dlxm1**2)/9.d0 +dli2a*(1 + (58*dlnx)/9.d0 +
     &     (20*dlxm1)/9.d0) +(-12*dlnx- (50*dlnx**2)/9.d0 + (224*dlnx
     &     *dlxm1)/9.d0)*lfh -(32*dlnx*lfh**2)/9.d0 + (38*dlnx*lfr)/3.d0
     &     + (164*dlnx*z2)/9.d0)/xm1 -(764*z3)/9.d0 

      deltabbqa = deltabbqa + xm1**2*(15.805555555555555d0 + (55*dli3a)
     &     /9.d0+ 2*dli3b -2*dli3c + (76*dli3d)/9.d0 - dli3f/6.d0 + (31
     &     *dlnx)/3.d0+(79*dlnx**2)/18.d0 + (11*dlnx**3)/9.d0 +dli2b*(0
     &     .8888888888888888d0 + dlnx) - (185*dlxm1)/9.d0 -(88*dlnx
     &     *dlxm1)/9.d0 - (110*dlnx**2*dlxm1)/9.d0 + (68*dlxm1**2)/9.d0
     &     +(56*dlnx*dlxm1**2)/3.d0 - (64*dlxm1**3)/9.d0 +dli2a*(2
     &     .2222222222222223d0 - (94*dlnx)/9.d0 + (26*dlxm1)/3.d0 -(44
     &     *lfh)/9.d0) + (2.7777777777777777d0+ 3*dlnx - (32*dlxm1)/9.d0
     &     )*lfh**2 + (6.333333333333333d0 - (19*dlnx)/3.d0 + (38*dlxm1)
     &     /3.d0)*lfr + lfh*(10.555555555555555d0 +(16*dlnx)/3.d0 + (43
     &     *dlnx**2)/9.d0 -(68*dlxm1)/9.d0 - (52*dlnx*dlxm1)/3.d0 + (32
     &     *dlxm1**2)/3.d0 -(19*lfr)/3.d0 - (50*z2)/9.d0) -(41*z2)/3.d0
     &     - 14*dlnx*z2 +(100*dlxm1*z2)/9.d0 - (191*z3)/9.d0 - (7*dli3e)
     &     /6.d0)

      deltabbqa = deltabbqa + xm1**3*(-2.2253086419753085d0 - (2*dli3a)
     &     /3.d0- (2*dli3b)/3.d0 + (2*dli3c)/3.d0 + (2*dli3d)/3.d0 +
     &     dli3f/18.d0 +dli2b*(-0.2222222222222222d0 - dlnx/3.d0) +
     &     dli2a*(-1.1111111111111112d0 + (2*dlnx)/9.d0) - (19*dlnx)/27
     &     .d0 + (2*dlnx**2)/3.d0 - (4*dlnx**3)/27.d0 + (44*dlxm1)/27.d0
     &     - (8*dlnx*dlxm1)/3.d0 +(8*dlxm1**2)/9.d0 + (-0
     &     .8148148148148148d0 + (4*dlnx)/3.d0 - (8*dlxm1)/9.d0)*lfh +
     &     (2*lfh**2)/9.d0 - (8*z2)/9.d0 + (7*dli3e)/18.d0)- (7*dli3e)
     &     /18.d0 + xm1*(-28.074074074074073d0 - (43*dli3a)/3.d0 - 2
     &     *dli3b + 2*dli3c - (88*dli3d)/3.d0 + dli3f/6.d0 + dli2b*(-1 -
     &     dlnx) - (139*dlnx)/6.d0 -(89*dlnx**2)/6.d0 - (25*dlnx**3)/9
     &     .d0 + (124*dlxm1)/3.d0 + (358*dlnx*dlxm1)/9.d0 + (110*dlnx**2
     &     *dlxm1)/3.d0 - (200*dlxm1**2)/9.d0- 56*dlnx*dlxm1**2 + (64
     &     *dlxm1**3)/3.d0 + (-6.444444444444445d0 -9*dlnx + (32*dlxm1)
     &     /3.d0)*lfh**2 + dli2a*(4.555555555555555d0 +30*dlnx - 26
     &     *dlxm1 + (44*lfh)/3.d0) + (-6.333333333333333d0 + 19*dlnx -
     &     38*dlxm1)*lfr + (85*z2)/3.d0 + 42*dlnx*z2 - (100*dlxm1*z2)/3
     &     .d0 + lfh*(-21.22222222222222d0 - (61*dlnx)/3.d0 - (43*dlnx
     &     **2)/3.d0 + (200*dlxm1)/9.d0 + 52*dlnx*dlxm1 - 32*dlxm1**2 +
     &     19*lfr + (50*z2)/3.d0) + (191*z3)/3.d0 + (7*dli3e)/6.d0)

      end

c-}}}
c-{{{ function deltabbqf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabbqf(xx,lfh,lfr)

      implicit none

      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog
      
      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0


      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabbqf = -1.382716049382716d0 + (2*dli2a)/9.d0 - (20*dlnx)/9.d0
     &     - (13*dlnx **2)/9.d0 +(80*dlxm1)/27.d0 + (32*dlnx*dlxm1)/9.d0
     &     - (16*dlxm1**2) /9.d0 -(4*lfh**2)/9.d0 + lfh*(-1
     &     .4814814814814814d0 -(16*dlnx)/9.d0 +(16*dlxm1)/9.d0 + (8*lfr
     &     )/9.d0) + ((8*dlnx)/9.d0 -(16*dlxm1)/9.d0) *lfr +((-2*dli2a)
     &     /9.d0 + (10*dlnx)/9.d0 + (2*dlnx**2)/3.d0 -(16 *dlnx*dlxm1)/9
     &     .d0 + (8*dlnx*lfh)/9.d0 - (4*dlnx*lfr)/9.d0)/xm1 +xm1 *(1
     &     .3703703703703705d0 + (17*dlnx)/9.d0 + (7*dlnx**2)/6.d0 -(8
     &     *dlxm1)/3.d0 - (8*dlnx*dlxm1)/3.d0 + (4*dlxm1**2)/3.d0+ lfh
     &     **2/3.d0 +lfh*(1.3333333333333333d0 + (4*dlnx)/3.d0 - (4
     &     *dlxm1)/3.d0 -(2*lfr)/3.d0) + (0.2222222222222222d0 - (2*dlnx
     &     )/3.d0 +(4*dlxm1)/3.d0)*lfr - (4*z2)/3.d0) +xm1**2*(-0
     &     .6790123456790124d0 -(7*dlnx)/9.d0 - (7 *dlnx**2)/18.d0 +(32
     &     *dlxm1)/27.d0 + (8*dlnx*dlxm1)/9.d0 - (4*dlxm1 **2)/9.d0 -lfh
     &     **2/9.d0 + lfh*(-0.5925925925925926d0 - (4*dlnx)/9.d0 +(4
     &     *dlxm1)/9.d0 + (2*lfr)/9.d0)+(-0.2222222222222222d0 + (2*dlnx
     &     ) /9.d0 - (4*dlxm1)/9.d0)*lfr + (4*z2)/9.d0) + (16*z2)/9.d0

      end

c-}}}
c-{{{ function deltabga:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabga(xx,lfh,lfr)

      implicit none
       
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog
      
      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0
      
      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabga = -0.7916666666666666d0 - (31*dli3a)/3.d0 - (15*dli3b)/2
     &     .d0 +(15*dli3c)/2.d0 + (113*dli3d)/12.d0 + (13*dli3e)/48.d0 +
     &     (13*dli3f)/48.d0 - (425*dlnx)/48.d0 - (157*dlnx**2)/32.d0 -
     &     (193*dlnx**3)/144.d0 + (13*dlxm1)/16.d0 + (331*dlnx*dlxm1)/24
     &     .d0 - (79*dlnx**2*dlxm1)/24.d0 - dlxm1**2/2.d0 + (21*dlnx
     &     *dlxm1**2)/8.d0 + (257*dlxm1**3)/144.d0 + dli2a*(16.375d0 -
     &     (9*dlnx)/2.d0 + (125*dlxm1)/24.d0 - (5*lfh)/2.d0) + dli2b*(-1
     &     .6666666666666667d0 - (37*dlnx)/12.d0 + (15*dlxm1)/4.d0 - (15
     &     *lfh)/8.d0) + (-0.125d0 + (9*dlnx)/8.d0 + (7*dlxm1)/8.d0)*lfh
     &     **2 + ((19*dlnx)/16.d0 - (19*dlxm1)/8.d0)*lfr + z2/2.d0 -(89
     &     *dlnx*z2)/24.d0 - (35*dlxm1*z2)/12.d0 + lfh* (-0
     &     .4583333333333333d0 - (163*dlnx)/24.d0 + 2*dlnx**2 + dlxm1/2
     &     .d0 -(37*dlnx*dlxm1)/12.d0 - (21*dlxm1**2)/8.d0 + (19*lfr)/16
     &     .d0 + (35*z2)/24.d0) 

      deltabga = deltabga + xm1*(7.75d0 + 19*dli3a + (33*dli3b)/2.d0 -
     &     (33*dli3c)/2.d0 - (65*dli3d)/4.d0 - dli3e/16.d0 - dli3f/16.d0
     &     + (625*dlnx)/16.d0 + (641*dlnx**2)/32.d0 + (121*dlnx**3)/48
     &     .d0 - (31*dlxm1)/2.d0 - (457*dlnx*dlxm1)/8.d0 + (19*dlnx**2
     &     *dlxm1)/8.d0 +(113*dlxm1**2)/8.d0 + (9*dlnx*dlxm1**2)/8.d0 -
     &     (257*dlxm1**3)/48.d0+ (3.25d0 - (9*dlnx)/8.d0 - (21*dlxm1)/8
     &     .d0)*lfh**2 + dli2a*(-40.375d0 + (9*dlnx)/2.d0 - (29*dlxm1)/8
     &     .d0 + (3*lfh)/2.d0) + dli2b*(3.375d0 + (25*dlnx)/4.d0 - (33
     &     *dlxm1)/4.d0 + (33*lfh)/8.d0) + (-2.375d0- (57*dlnx)/16.d0 +
     &     (57*dlxm1)/8.d0)*lfr + lfh*(8.166666666666666d0+ (227*dlnx)/8
     &     .d0 - (9*dlnx**2)/4.d0 - (43*dlxm1)/3.d0 + (dlnx*dlxm1)/4.d0
     &     + (63*dlxm1**2)/8.d0 - (57*lfr)/16.d0 - (35*z2)/8.d0) -(93*z2
     &     )/8.d0 + (17*dlnx*z2)/8.d0 + (35*dlxm1*z2)/4.d0 - (161*z3)/16
     &     .d0)

      deltabga = deltabga + xm1**3*(6.184027777777778d0 + 3*dli3a + 3
     &     *dli3b - 3*dli3c + (3*dli3d)/2.d0 + (7*dli3e)/24.d0 + (7
     &     *dli3f) /24.d0 + (331*dlnx)/16.d0 + (257*dlnx**2)/24.d0 +
     &     dlnx**3/12.d0 - (121*dlxm1)/8.d0 - (129*dlnx*dlxm1)/4.d0 -
     &     (49*dlnx**2*dlxm1)/12.d0 + (237*dlxm1**2)/16.d0 + (31*dlnx
     &     *dlxm1**2)/4.d0 - (257*dlxm1**3) /72.d0 + dli2a*(-9
     &     .833333333333334d0 - (17*dlnx)/6.d0 + (43*dlxm1) /12.d0 - 2
     &     *lfh) + dli2b*(0.4375d0 + (5*dlnx)/6.d0 - (3*dlxm1)/2.d0 + (3
     &     *lfh)/4.d0) + (2.9375d0 + dlnx - (7*dlxm1)/4.d0)*lfh**2 + (-4
     &     .15625d0 - (19*dlnx)/8.d0 + (19*dlxm1)/4.d0)*lfr + lfh*(7
     &     .270833333333333d0 + (46*dlnx)/3.d0 + (4*dlnx**2)/3.d0 - (353
     &     *dlxm1)/24.d0 - (41*dlnx*dlxm1)/6.d0 + (21*dlxm1**2)/4.d0 -
     &     (19*lfr)/8.d0 - (35*z2)/12.d0) - (169*z2)/16.d0 - (67*dlnx*z2
     &     )/12.d0 + (35*dlxm1*z2)/6.d0 - (161*z3)/24.d0) + (161*z3)/48
     &     .d0 

      deltabga = deltabga + xm1**2*(-13.864583333333334d0 - (35*dli3a)/3
     &     .d0- 12*dli3b + 12*dli3c + (16*dli3d)/3.d0 - dli3e/2.d0 -
     &     dli3f/2.d0 -(2443*dlnx)/48.d0 - (155*dlnx**2)/6.d0 - (91*dlnx
     &     **3)/72.d0 + (1447*dlxm1)/48.d0 + (907*dlnx*dlxm1)/12.d0 + 5
     &     *dlnx**2*dlxm1 - (439*dlxm1**2)/16.d0 - (23*dlnx*dlxm1**2)/2
     &     .d0 + (257*dlxm1**3)/36.d0 +dli2b*(-2.1458333333333335d0 - 4
     &     *dlnx + 6*dlxm1 - 3*lfh) + (-5.8125d0 - dlnx + (7*dlxm1)/2.d0
     &     )*lfh**2 + dli2a*(35.833333333333336d0 + (17*dlnx)/6.d0 - (31
     &     *dlxm1)/6.d0 + 3*lfh) + (6.53125d0 + (19*dlnx)/4.d0 - (19
     &     *dlxm1)/2.d0)*lfr + (331*z2)/16.d0 +(43*dlnx*z2)/6.d0 - (35
     &     *dlxm1*z2)/3.d0 + lfh*(-15.145833333333334d0- (443*dlnx)/12
     &     .d0 - (13*dlnx**2)/12.d0 + (661*dlxm1)/24.d0 + (29*dlnx*dlxm1
     &     )/3.d0 - (21*dlxm1**2)/2.d0 + (19*lfr)/4.d0 + (35*z2)/6.d0) +
     &     (161*z3)/12.d0)

      end

c-}}}
c-{{{ function deltabgf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabgf(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog
      
      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabgf = (-dlnx/24.d0 + dlxm1/12.d0)*lfr - (lfh*lfr)/24.d0 + ((0
     &     .08333333333333333d0 + dlnx/8.d0 - dlxm1/4.d0)*lfr + (lfh*lfr
     &     )/8.d0) * xm1 + ((-0.22916666666666666d0 - dlnx/6.d0 + dlxm1
     &     /3.d0)*lfr - (lfh*lfr)/6.d0)*xm1**2 + ((0.14583333333333334d0
     &     + dlnx/12.d0 - dlxm1/6.d0)*lfr + (lfh*lfr)/12.d0)* xm1**3

      end

c-}}}
c-{{{ function deltagga:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltagga(xx,lfh,lfr)
      
      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog
      
      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0
      
      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltagga = (11*dli3a)/4.d0 + (5*dli3b)/32.d0 - (5*dli3c)/32.d0 -
     &     (105*dli3d)/16.d0 -(89*dli3e)/128.d0 - (79*dli3f)/128.d0
     &     +dli2b*(-0.15625d0 + (47*dlnx)/64.d0) - (143*dlnx)/64.d0 -(21
     &     *dlnx**2)/16.d0 +(11*dlnx**3)/8.d0 + (17*dlnx*dlxm1)/4.d0 +(9
     &     *dlnx**2*dlxm1)/4.d0 -(9*dlnx*dlxm1**2)/2.d0 +((-17*dlnx)/8
     &     .d0 - (9*dlnx**2)/8.d0 + (9*dlnx*dlxm1)/2.d0)*lfh -(9*dlnx
     &     *lfh**2)/8.d0 + dli2a*(4.5625d0 +(85*dlnx)/16.d0 - 9*dlxm1 +
     &     (9*lfh)/2.d0) + (9*dlnx*z2)/2.d0 +xm1*(-14.484375d0 - (61
     &     *dli3a)/8.d0 - (11*dli3b)/32.d0 + (11*dli3c)/32.d0+(225*dli3d
     &     )/16.d0 + (247*dli3e)/128.d0 + (225*dli3f)/128.d0 +dli2b*(0
     &     .5d0 - (129*dlnx)/64.d0) + (9*dlnx)/64.d0 + (109*dlnx**2)/64
     &     .d0-(53*dlnx**3)/16.d0 + (49*dlxm1)/4.d0 - (25*dlnx*dlxm1)/4
     &     .d0 -(21*dlnx**2*dlxm1)/4.d0 - 4*dlxm1**2 + (21*dlnx*dlxm1**2
     &     )/2.d0+dli2a*(-13.25d0 - (189*dlnx)/16.d0 + 21*dlxm1 - (21
     &     *lfh)/2.d0) +(-6.125d0 + (25*dlnx)/8.d0 + (21*dlnx**2)/8.d0 +
     &     4*dlxm1 -(21*dlnx*dlxm1)/2.d0)*lfh + (-1 + (21*dlnx)/8.d0)
     &     *lfh**2 + 4*z2 -(21*dlnx*z2)/2.d0) 

      deltagga = deltagga + xm1**3* (-13.2109375d0 - (9*dli3a)/4.d0 -
     &     dli3b/16.d0 + dli3c/16.d0 + (15*dli3d)/8.d0 + (37*dli3e)/64
     &     .d0 + (35*dli3f)/64.d0 + dli2b*(0.1875d0 - (19*dlnx)/32.d0) -
     &     (223*dlnx)/64.d0 - (43*dlnx**2)/64.d0 - (17*dlnx**3)/24.d0 +
     &     (75*dlxm1)/8.d0 + dlnx*dlxm1- dlnx**2*dlxm1 - 3*dlxm1**2 + 2
     &     *dlnx*dlxm1**2 + dli2a*(-3.875d0 - (15*dlnx)/8.d0 + 4*dlxm1 -
     &     2*lfh) + (-4.6875d0 - dlnx/2.d0 +dlnx**2/2.d0 + 3*dlxm1 - 2
     &     *dlnx*dlxm1)*lfh + (-0.75d0 + dlnx/2.d0)*lfh**2 + 3*z2 - 2
     &     *dlnx*z2)

      deltagga = deltagga + xm1**2*(27.6953125d0 + (57*dli3a)/8.d0 +
     &     dli3b/4.d0 - dli3c/4.d0 -(75*dli3d)/8.d0 - (29*dli3e)/16.d0 -
     &     (27*dli3f)/16.d0+(357*dlnx)/64.d0 + (9*dlnx**2)/32.d0 + (127
     &     *dlnx**3)/48.d0 +dli2b*(-0.53125d0 + (15*dlnx)/8.d0) - (173
     &     *dlxm1)/8.d0 + dlnx*dlxm1 +4*dlnx**2*dlxm1 + 7*dlxm1**2 - 8
     &     *dlnx*dlxm1**2 +(10.8125d0 -dlnx/2.d0 - 2*dlnx**2 - 7*dlxm1 +
     &     8*dlnx*dlxm1)*lfh +(1.75d0 - 2*dlnx)*lfh**2 +dli2a*(12.5625d0
     &     + (67*dlnx)/8.d0 - 16*dlxm1 + 8*lfh) - 7*z2 +8*dlnx*z2)

      end

c-}}}
c-{{{ function deltaggf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltaggf(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltaggf = 0.d0

      end

c-}}}
c-{{{ function deltabba:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabba(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabba = (31*dli2a)/9.d0 + dli2b/9.d0 - (10*dli3a)/9.d0 + (20
     &     *dli3b)/9.d0 -(20*dli3c)/9.d0 + (5*dli3d)/3.d0 - dli3e/3.d0 -
     &     dli3f/9.d0 - (71*dlnx)/27.d0 -(49*dli2a*dlnx)/9.d0 + (8*dli2b
     &     *dlnx)/9.d0 - (23*dlnx**2)/18.d0 +(11*dlnx**3)/18.d0 + (64
     &     *dli2a*dlxm1)/9.d0 - (8*dli2b*dlxm1)/9.d0 +(34*dlnx*dlxm1)/9
     &     .d0 - (28*dlnx**2*dlxm1)/9.d0 + (8*dlnx*dlxm1**2)/3.d0 -(32
     &     *dli2a*lfh)/9.d0 + (4*dli2b*lfh)/9.d0 -(17*dlnx*lfh)/9.d0
     &     +(14*dlnx**2*lfh)/9.d0 - (8*dlnx*dlxm1*lfh)/3.d0+ (2*dlnx*lfh
     &     **2)/3.d0 -(4*dli3a)/(3.d0*(2 - xm1)) - (20*dli3b)/(9.d0*(2 -
     &     xm1)) +(20*dli3c)/(9.d0*(2 - xm1)) + (4*dli3d)/(3.d0*(2- xm1)
     &     ) +dli3e/(3.d0*(2 - xm1)) + dli3f/(9.d0*(2 - xm1)) +(4*dli2a
     &     *dlnx)/(3.d0*(2 - xm1)) - (8*dli2b*dlnx)/(9.d0*(2 - xm1))-(14
     &     *dlnx**3)/(27.d0*(2 - xm1)) - (16*dli2a*dlxm1)/(9.d0*(2 - xm1
     &     )) +(8*dli2b*dlxm1)/(9.d0*(2 - xm1)) + (4*dlnx**2*dlxm1)/(9
     &     .d0*(2- xm1)) +(8*dli2a*lfh)/(9.d0*(2 - xm1)) - (4*dli2b*lfh)
     &     /(9.d0*(2- xm1)) -(2*dlnx**2*lfh)/(9.d0*(2 - xm1)) + (16*xm1)
     &     /27.d0 - (53*dli2a*xm1)/9.d0 -(dli2b*xm1)/9.d0 + (17*dli3a
     &     *xm1)/9.d0 - (13*dli3b*xm1)/9.d0 +(13*dli3c*xm1)/9.d0 - (40
     &     *dli3d*xm1)/9.d0 + (13*dli3e*xm1)/36.d0 +(dli3f*xm1)/12.d0 

      deltabba = deltabba + (127*dlnx*xm1)/18.d0 + (22*dli2a*dlnx*xm1)/3
     &     .d0-(11*dli2b*dlnx*xm1)/18.d0 + (125*dlnx**2*xm1)/36.d0 -(11
     &     *dlnx**3*xm1)/27.d0 - (26*dlxm1*xm1)/9.d0 - (80*dli2a*dlxm1
     &     *xm1)/9.d0 +(4*dli2b*dlxm1*xm1)/9.d0 - (34*dlnx*dlxm1*xm1)/3
     &     .d0 +(38*dlnx**2*dlxm1*xm1)/9.d0 + (10*dlxm1**2*xm1)/3.d0 -4
     &     *dlnx*dlxm1**2*xm1 +(13*lfh*xm1)/9.d0 + (40*dli2a*lfh*xm1)/9
     &     .d0 -(2*dli2b*lfh*xm1)/9.d0+ (17*dlnx*lfh*xm1)/3.d0 -(19*dlnx
     &     **2*lfh*xm1)/9.d0 - (10*dlxm1*lfh*xm1)/3.d0 +4*dlnx*dlxm1*lfh
     &     *xm1 + (5*lfh**2*xm1)/6.d0 - dlnx*lfh**2*xm1 +(35*xm1**2)/27
     &     .d0 + (49*dli2a*xm1**2)/9.d0 + (5*dli3a*xm1**2)/9.d0 +(13
     &     *dli3b*xm1**2)/9.d0 - (13*dli3c*xm1**2)/9.d0 + (5*dli3d*xm1
     &     **2)/3.d0 -(13*dli3e*xm1**2)/36.d0 - (dli3f*xm1**2)/12.d0 -
     &     (73*dlnx*xm1**2)/18.d0 -(29*dli2a*dlnx*xm1**2)/9.d0 +(11
     &     *dli2b*dlnx*xm1**2)/18.d0 -(113*dlnx**2*xm1**2)/36.d0 + (17
     &     *dlnx**3*xm1**2)/54.d0 +(14*dlxm1*xm1**2)/9.d0 + (32*dli2a
     &     *dlxm1*xm1**2)/9.d0 -(4*dli2b*dlxm1*xm1**2)/9.d0 + (92*dlnx
     &     *dlxm1*xm1**2)/9.d0 -(14*dlnx**2*dlxm1*xm1**2)/9.d0 - (10
     &     *dlxm1**2*xm1**2)/3.d0 +(4*dlnx*dlxm1**2*xm1**2)/3.d0 - (7
     &     *lfh*xm1**2)/9.d0 -(16*dli2a*lfh*xm1**2)/9.d0 

      deltabba = deltabba + (2*dli2b*lfh*xm1**2)/9.d0 - (46*dlnx*lfh*xm1
     &     **2)/9.d0 + (7*dlnx**2*lfh*xm1**2)/9.d0 + (10*dlxm1*lfh*xm1
     &     **2)/3 .d0 - (4*dlnx*dlxm1*lfh*xm1**2)/3.d0 - (5*lfh**2*xm1
     &     **2)/6.d0 + (dlnx*lfh**2*xm1**2)/3.d0 - (205*xm1**3)/81.d0 -
     &     (11*dli2a*xm1**3)/9.d0 - (2*dli3d*xm1**3)/9.d0 - (10*dlnx*xm1
     &     **3)/27.d0 + (17*dlnx **2*xm1**3)/18.d0 + (44*dlxm1*xm1**3)
     &     /27.d0 - (8*dlnx*dlxm1*xm1 **3)/3.d0 + (8*dlxm1**2*xm1**3)/9
     &     .d0 - (22*lfh*xm1**3)/27.d0 + (4 *dlnx*lfh*xm1**3)/3.d0 - (8
     &     *dlxm1*lfh*xm1**3)/9.d0 + (2*lfh**2 *xm1**3)/9.d0 - (8*dlnx
     &     *z2)/3.d0 - (10*xm1*z2)/3.d0 + 4*dlnx*xm1*z2 + (10*xm1**2*z2)
     &     /3.d0 - (4*dlnx*xm1**2*z2)/3.d0 - (8*xm1**3*z2)/9 .d0


      end

c-}}}
c-{{{ function deltabbf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabbf(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabbf = 0.d0

      end

c-}}}
c-{{{ function deltabqa:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabqa(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabqa = (-4*dli3a)/3.d0 + (4*dli3d)/3.d0 - (151*dlnx)/108.d0 -
     &     (29*dlnx**2)/36.d0 +dlnx**3/18.d0 + (7*dlnx*dlxm1)/3.d0 - (4
     &     *dlnx**2*dlxm1)/3.d0 +(4*dlnx*dlxm1**2)/3.d0 +dli2a*(2
     &     .3333333333333335d0 -2*dlnx + (8*dlxm1)/3.d0 - (4*lfh)/3.d0)
     &     +((-7*dlnx)/6.d0 + (2*dlnx**2)/3.d0 - (4*dlnx*dlxm1)/3.d0)
     &     *lfh +(dlnx*lfh**2)/3.d0 + xm1**3*(-1.0848765432098766d0 - (4
     &     *dli2a)/9.d0 - (5*dlnx)/27.d0 +(5*dlnx**2)/9.d0 + (22*dlxm1)
     &     /27.d0 - (4*dlnx*dlxm1)/3.d0 +(4*dlxm1**2)/9.d0 +(-0
     &     .4074074074074074d0 + (2*dlnx)/3.d0 -(4*dlxm1)/9.d0)*lfh +
     &     lfh**2/9.d0 - (4*z2)/9.d0) - (4*dlnx*z2)/3.d0 +xm1**2*(1
     &     .0787037037037037d0 - (2*dli3a)/3.d0 + (2*dli3d)/3.d0 -(17
     &     *dlnx)/9.d0- (15*dlnx**2)/8.d0 + dlnx**3/36.d0 + dlxm1/3.d0
     &     +(16*dlnx*dlxm1)/3.d0 - (2*dlnx**2*dlxm1)/3.d0 - (5*dlxm1**2)
     &     /3.d0 +(2*dlnx*dlxm1**2)/3.d0 +dli2a*(2.5d0 - dlnx + (4*dlxm1
     &     )/3.d0 - (2*lfh)/3.d0) +(-0.16666666666666666d0 - (8*dlnx)/3
     &     .d0 + dlnx**2/3.d0 + (5*dlxm1)/3.d0-(2*dlnx*dlxm1)/3.d0)*lfh
     &     +(-0.4166666666666667d0 + dlnx/6.d0)*lfh**2 + (5*z2)/3.d0 -(2
     &     *dlnx*z2)/3.d0) 

      deltabqa = deltabqa + xm1* (-0.3148148148148148d0 + 2*dli3a - 2
     &     *dli3d + (125*dlnx)/36.d0 + (17*dlnx**2)/8.d0 - dlnx**3/12.d0
     &     - dlxm1 - (19*dlnx*dlxm1)/3.d0 + 2*dlnx**2*dlxm1 + (5*dlxm1
     &     **2)/3 .d0 - 2*dlnx*dlxm1**2 + (0.5d0 + (19*dlnx)/6.d0 - dlnx
     &     **2 - (5 *dlxm1)/3.d0 + 2*dlnx*dlxm1)* lfh + (0
     &     .4166666666666667d0 - dlnx /2.d0)*lfh**2 + dli2a*(-3.5d0 + 3
     &     *dlnx - 4*dlxm1 + 2*lfh) - (5*z2)/3.d0 + 2*dlnx*z2)

      end

c-}}}
c-{{{ function deltabqf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltabqf(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltabqf = 0.d0

      end

c-}}}
c-{{{ function deltaqqba:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltaqqba(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltaqqba = (-4*dli2a)/9.d0 + (2*dli2b)/9.d0 + (4*dlnx)/9.d0 +
     &     dlnx**2/9.d0 +(0.4444444444444444d0 + (4*dli2a)/3.d0 - (2
     &     *dli2b)/3.d0 - (4*dlnx)/3.d0 -dlnx**2/3.d0)*xm1 + (-1
     &     .1111111111111112d0 - (4*dli2a)/3.d0 +(2*dli2b)/3.d0 + (11
     &     *dlnx)/9.d0 + dlnx**2/3.d0)*xm1**2 +(0.6666666666666666d0 +
     &     (4*dli2a)/9.d0 - (2*dli2b)/9.d0 - dlnx/3.d0-dlnx**2/9.d0)*xm1
     &     **3

      end

c-}}}
c-{{{ function deltaqqbf:
c.......................................................................
c............  Checked .................................................
c.......................................................................
      double precision function deltaqqbf(xx,lfh,lfr)

      implicit none
      double precision xx,lfh,lfr
      double precision xm1,dlnx,dlxm1
      double precision dli2a,dli2b,dli3a,dli3b
      double precision dli3c,dli3d,dli3e,dli3f
      double precision z2,z3,z4
      
      double precision dilog
      double precision trilog
      external dilog,trilog

      z2 = 1.6449340668482264365d0
      z3 = 1.2020569031595942854d0
      z4 = 1.0823232337111381915d0

      xm1 = 1.d0 - xx
      dlnx = dlog(xx)
      dlxm1 = dlog(xm1)

      dli2a = dilog(xm1)
      dli2b = dilog(1.d0-xx**2)
      dli3a = trilog(xm1)
      dli3b = trilog(-(xm1/(2.d0 - xm1)))
      dli3c = trilog(xm1/(2.d0 - xm1))
      dli3d = trilog(-(xm1/xx)) 
      dli3e = trilog(1.d0 - xx**2)
      dli3f = trilog(-((1.d0 - xx**2)/xx**2))

      deltaqqbf = 0.d0

      end

c-}}}
c......................................................................
c......................................................................
c................... FUNCTIONS ........................................
c......................................................................
c......................................................................
      double precision function dilog(xx)
c..
c..   Dilogarithm: dilog(x) = Li_2(x)
c..
      double precision xx
      double precision wgplg
      dilog = wgplg(1,1,xx)
      end

c..   ------------------------------------------------------------

C-}}}
C-{{{ function trilog:

c..   ------------------------------------------------------------

      double precision function trilog(xx)
c..
c..   Trilogarithm: trilog(x) = Li_3(x)
c..
      double precision xx
      double precision wgplg
      trilog = wgplg(2,1,xx)
      end

c..   ------------------------------------------------------------

C-}}}
C-{{{ FUNCTION WGPLG:

      double precision FUNCTION WGPLG(N,P,X)
c..
c..   Nielson's Generalized Polylogarithm
c..
c..   taken from zwprod.f (R. Hamberg, T. Matsuura and W.L. van Neerven)
c..   (originally from CERNLIB?)
c..
      INTEGER P,P1,NC(10),INDEX(31)
      DOUBLE PRECISION FCT(0:4),SGN(0:4),U(0:4),S1(4,4),C(4,4)
      DOUBLE PRECISION A(0:30,10)
      DOUBLE PRECISION X,X1,H,ALFA,R,Q,C1,C2,B0,B1,B2,ZERO,HALF

      DOUBLE PRECISION V(0:5),SK,SM

      DATA FCT /1.0D0,1.0D0,2.0D0,6.0D0,24.0D0/
      DATA SGN /1.0D0,-1.0D0,1.0D0,-1.0D0,1.0D0/
      DATA ZERO /0.0D0/, HALF /0.5D0/
      DATA C1 /1.33333 33333 333D0/, C2 /0.33333 33333 3333D0/

      DATA S1(1,1) /1.64493 40668 482D0/
      DATA S1(1,2) /1.20205 69031 596D0/
      DATA S1(1,3) /1.08232 32337 111D0/
      DATA S1(1,4) /1.03692 77551 434D0/
      DATA S1(2,1) /1.20205 69031 596D0/
      DATA S1(2,2) /2.70580 80842 778D-1/
      DATA S1(2,3) /9.65511 59989 444D-2/
      DATA S1(3,1) /1.08232 32337 111D0/
      DATA S1(3,2) /9.65511 59989 444D-2/
      DATA S1(4,1) /1.03692 77551 434D0/

      DATA C(1,1) / 1.64493 40668 482D0/
      DATA C(1,2) / 1.20205 69031 596D0/
      DATA C(1,3) / 1.08232 32337 111D0/
      DATA C(1,4) / 1.03692 77551 434D0/
      DATA C(2,1) / 0.00000 00000 000D0/
      DATA C(2,2) /-1.89406 56589 945D0/
      DATA C(2,3) /-3.01423 21054 407D0/
      DATA C(3,1) / 1.89406 56589 945D0/
      DATA C(3,2) / 3.01423 21054 407D0/
      DATA C(4,1) / 0.00000 00000 000D0/

      DATA INDEX /1,2,3,4,6*0,5,6,7,7*0,8,9,8*0,10/

      DATA NC /24,26,28,30,22,24,26,19,22,17/

      DATA A( 0,1) / .96753 21504 3498D0/
      DATA A( 1,1) / .16607 30329 2785D0/
      DATA A( 2,1) / .02487 93229 2423D0/
      DATA A( 3,1) / .00468 63619 5945D0/
      DATA A( 4,1) / .00100 16274 9616D0/
      DATA A( 5,1) / .00023 20021 9609D0/
      DATA A( 6,1) / .00005 68178 2272D0/
      DATA A( 7,1) / .00001 44963 0056D0/
      DATA A( 8,1) / .00000 38163 2946D0/
      DATA A( 9,1) / .00000 10299 0426D0/
      DATA A(10,1) / .00000 02835 7538D0/
      DATA A(11,1) / .00000 00793 8705D0/
      DATA A(12,1) / .00000 00225 3670D0/
      DATA A(13,1) / .00000 00064 7434D0/
      DATA A(14,1) / .00000 00018 7912D0/
      DATA A(15,1) / .00000 00005 5029D0/
      DATA A(16,1) / .00000 00001 6242D0/
      DATA A(17,1) / .00000 00000 4827D0/
      DATA A(18,1) / .00000 00000 1444D0/
      DATA A(19,1) / .00000 00000 0434D0/
      DATA A(20,1) / .00000 00000 0131D0/
      DATA A(21,1) / .00000 00000 0040D0/
      DATA A(22,1) / .00000 00000 0012D0/
      DATA A(23,1) / .00000 00000 0004D0/
      DATA A(24,1) / .00000 00000 0001D0/

      DATA A( 0,2) / .95180 88912 7832D0/
      DATA A( 1,2) / .43131 13184 6532D0/
      DATA A( 2,2) / .10002 25071 4905D0/
      DATA A( 3,2) / .02442 41559 5220D0/
      DATA A( 4,2) / .00622 51246 3724D0/
      DATA A( 5,2) / .00164 07883 1235D0/
      DATA A( 6,2) / .00044 40792 0265D0/
      DATA A( 7,2) / .00012 27749 4168D0/
      DATA A( 8,2) / .00003 45398 1284D0/
      DATA A( 9,2) / .00000 98586 9565D0/
      DATA A(10,2) / .00000 28485 6995D0/
      DATA A(11,2) / .00000 08317 0847D0/
      DATA A(12,2) / .00000 02450 3950D0/
      DATA A(13,2) / .00000 00727 6496D0/
      DATA A(14,2) / .00000 00217 5802D0/
      DATA A(15,2) / .00000 00065 4616D0/
      DATA A(16,2) / .00000 00019 8033D0/
      DATA A(17,2) / .00000 00006 0204D0/
      DATA A(18,2) / .00000 00001 8385D0/
      DATA A(19,2) / .00000 00000 5637D0/
      DATA A(20,2) / .00000 00000 1735D0/
      DATA A(21,2) / .00000 00000 0536D0/
      DATA A(22,2) / .00000 00000 0166D0/
      DATA A(23,2) / .00000 00000 0052D0/
      DATA A(24,2) / .00000 00000 0016D0/
      DATA A(25,2) / .00000 00000 0005D0/
      DATA A(26,2) / .00000 00000 0002D0/

      DATA A( 0,3) / .98161 02799 1365D0/
      DATA A( 1,3) / .72926 80632 0726D0/
      DATA A( 2,3) / .22774 71490 9321D0/
      DATA A( 3,3) / .06809 08329 6197D0/
      DATA A( 4,3) / .02013 70118 3064D0/
      DATA A( 5,3) / .00595 47848 0197D0/
      DATA A( 6,3) / .00176 76901 3959D0/
      DATA A( 7,3) / .00052 74821 8502D0/
      DATA A( 8,3) / .00015 82746 1460D0/
      DATA A( 9,3) / .00004 77492 2076D0/
      DATA A(10,3) / .00001 44792 0408D0/
      DATA A(11,3) / .00000 44115 4886D0/
      DATA A(12,3) / .00000 13500 3870D0/
      DATA A(13,3) / .00000 04148 1779D0/
      DATA A(14,3) / .00000 01279 3307D0/
      DATA A(15,3) / .00000 00395 9070D0/
      DATA A(16,3) / .00000 00122 9055D0/
      DATA A(17,3) / .00000 00038 2658D0/
      DATA A(18,3) / .00000 00011 9459D0/
      DATA A(19,3) / .00000 00003 7386D0/
      DATA A(20,3) / .00000 00001 1727D0/
      DATA A(21,3) / .00000 00000 3687D0/
      DATA A(22,3) / .00000 00000 1161D0/
      DATA A(23,3) / .00000 00000 0366D0/
      DATA A(24,3) / .00000 00000 0116D0/
      DATA A(25,3) / .00000 00000 0037D0/
      DATA A(26,3) / .00000 00000 0012D0/
      DATA A(27,3) / .00000 00000 0004D0/
      DATA A(28,3) / .00000 00000 0001D0/

      DATA A( 0,4) /1.06405 21184 614 D0/
      DATA A( 1,4) /1.06917 20744 981 D0/
      DATA A( 2,4) / .41527 19325 1768D0/
      DATA A( 3,4) / .14610 33293 6222D0/
      DATA A( 4,4) / .04904 73264 8784D0/
      DATA A( 5,4) / .01606 34086 0396D0/
      DATA A( 6,4) / .00518 88935 0790D0/
      DATA A( 7,4) / .00166 29871 7324D0/
      DATA A( 8,4) / .00053 05827 9969D0/
      DATA A( 9,4) / .00016 88702 9251D0/
      DATA A(10,4) / .00005 36832 8059D0/
      DATA A(11,4) / .00001 70592 3313D0/
      DATA A(12,4) / .00000 54217 4374D0/
      DATA A(13,4) / .00000 17239 4082D0/
      DATA A(14,4) / .00000 05485 3275D0/
      DATA A(15,4) / .00000 01746 7795D0/
      DATA A(16,4) / .00000 00556 7550D0/
      DATA A(17,4) / .00000 00177 6234D0/
      DATA A(18,4) / .00000 00056 7224D0/
      DATA A(19,4) / .00000 00018 1313D0/
      DATA A(20,4) / .00000 00005 8012D0/
      DATA A(21,4) / .00000 00001 8579D0/
      DATA A(22,4) / .00000 00000 5955D0/
      DATA A(23,4) / .00000 00000 1911D0/
      DATA A(24,4) / .00000 00000 0614D0/
      DATA A(25,4) / .00000 00000 0197D0/
      DATA A(26,4) / .00000 00000 0063D0/
      DATA A(27,4) / .00000 00000 0020D0/
      DATA A(28,4) / .00000 00000 0007D0/
      DATA A(29,4) / .00000 00000 0002D0/
      DATA A(30,4) / .00000 00000 0001D0/

      DATA A( 0,5) / .97920 86066 9175D0/
      DATA A( 1,5) / .08518 81314 8683D0/
      DATA A( 2,5) / .00855 98522 2013D0/
      DATA A( 3,5) / .00121 17721 4413D0/
      DATA A( 4,5) / .00020 72276 8531D0/
      DATA A( 5,5) / .00003 99695 8691D0/
      DATA A( 6,5) / .00000 83806 4065D0/
      DATA A( 7,5) / .00000 18684 8945D0/
      DATA A( 8,5) / .00000 04366 6087D0/
      DATA A( 9,5) / .00000 01059 1733D0/
      DATA A(10,5) / .00000 00264 7892D0/
      DATA A(11,5) / .00000 00067 8700D0/
      DATA A(12,5) / .00000 00017 7654D0/
      DATA A(13,5) / .00000 00004 7342D0/
      DATA A(14,5) / .00000 00001 2812D0/
      DATA A(15,5) / .00000 00000 3514D0/
      DATA A(16,5) / .00000 00000 0975D0/
      DATA A(17,5) / .00000 00000 0274D0/
      DATA A(18,5) / .00000 00000 0077D0/
      DATA A(19,5) / .00000 00000 0022D0/
      DATA A(20,5) / .00000 00000 0006D0/
      DATA A(21,5) / .00000 00000 0002D0/
      DATA A(22,5) / .00000 00000 0001D0/

      DATA A( 0,6) / .95021 85196 3952D0/
      DATA A( 1,6) / .29052 52916 1433D0/
      DATA A( 2,6) / .05081 77406 1716D0/
      DATA A( 3,6) / .00995 54376 7280D0/
      DATA A( 4,6) / .00211 73389 5031D0/
      DATA A( 5,6) / .00047 85947 0550D0/
      DATA A( 6,6) / .00011 33432 1308D0/
      DATA A( 7,6) / .00002 78473 3104D0/
      DATA A( 8,6) / .00000 70478 8108D0/
      DATA A( 9,6) / .00000 18278 8740D0/
      DATA A(10,6) / .00000 04838 7492D0/
      DATA A(11,6) / .00000 01303 3842D0/
      DATA A(12,6) / .00000 00356 3769D0/
      DATA A(13,6) / .00000 00098 7174D0/
      DATA A(14,6) / .00000 00027 6586D0/
      DATA A(15,6) / .00000 00007 8279D0/
      DATA A(16,6) / .00000 00002 2354D0/
      DATA A(17,6) / .00000 00000 6435D0/
      DATA A(18,6) / .00000 00000 1866D0/
      DATA A(19,6) / .00000 00000 0545D0/
      DATA A(20,6) / .00000 00000 0160D0/
      DATA A(21,6) / .00000 00000 0047D0/
      DATA A(22,6) / .00000 00000 0014D0/
      DATA A(23,6) / .00000 00000 0004D0/
      DATA A(24,6) / .00000 00000 0001D0/

      DATA A( 0,7) / .95064 03218 6777D0/
      DATA A( 1,7) / .54138 28546 5171D0/
      DATA A( 2,7) / .13649 97959 0321D0/
      DATA A( 3,7) / .03417 94232 8207D0/
      DATA A( 4,7) / .00869 02788 3583D0/
      DATA A( 5,7) / .00225 28408 4155D0/
      DATA A( 6,7) / .00059 51608 9806D0/
      DATA A( 7,7) / .00015 99561 7766D0/
      DATA A( 8,7) / .00004 36521 3096D0/
      DATA A( 9,7) / .00001 20747 4688D0/
      DATA A(10,7) / .00000 33801 8176D0/
      DATA A(11,7) / .00000 09563 2476D0/
      DATA A(12,7) / .00000 02731 3129D0/
      DATA A(13,7) / .00000 00786 6968D0/
      DATA A(14,7) / .00000 00228 3195D0/
      DATA A(15,7) / .00000 00066 7205D0/
      DATA A(16,7) / .00000 00019 6191D0/
      DATA A(17,7) / .00000 00005 8018D0/
      DATA A(18,7) / .00000 00001 7246D0/
      DATA A(19,7) / .00000 00000 5151D0/
      DATA A(20,7) / .00000 00000 1545D0/
      DATA A(21,7) / .00000 00000 0465D0/
      DATA A(22,7) / .00000 00000 0141D0/
      DATA A(23,7) / .00000 00000 0043D0/
      DATA A(24,7) / .00000 00000 0013D0/
      DATA A(25,7) / .00000 00000 0004D0/
      DATA A(26,7) / .00000 00000 0001D0/

      DATA A( 0,8) / .98800 01167 2229D0/
      DATA A( 1,8) / .04364 06760 9601D0/
      DATA A( 2,8) / .00295 09117 8278D0/
      DATA A( 3,8) / .00031 47780 9720D0/
      DATA A( 4,8) / .00004 31484 6029D0/
      DATA A( 5,8) / .00000 69381 8230D0/
      DATA A( 6,8) / .00000 12464 0350D0/
      DATA A( 7,8) / .00000 02429 3628D0/
      DATA A( 8,8) / .00000 00504 0827D0/
      DATA A( 9,8) / .00000 00109 9075D0/
      DATA A(10,8) / .00000 00024 9467D0/
      DATA A(11,8) / .00000 00005 8540D0/
      DATA A(12,8) / .00000 00001 4127D0/
      DATA A(13,8) / .00000 00000 3492D0/
      DATA A(14,8) / .00000 00000 0881D0/
      DATA A(15,8) / .00000 00000 0226D0/
      DATA A(16,8) / .00000 00000 0059D0/
      DATA A(17,8) / .00000 00000 0016D0/
      DATA A(18,8) / .00000 00000 0004D0/
      DATA A(19,8) / .00000 00000 0001D0/

      DATA A( 0,9) / .95768 50654 6350D0/
      DATA A( 1,9) / .19725 24967 9534D0/
      DATA A( 2,9) / .02603 37031 3918D0/
      DATA A( 3,9) / .00409 38216 8261D0/
      DATA A( 4,9) / .00072 68170 7110D0/
      DATA A( 5,9) / .00014 09187 9261D0/
      DATA A( 6,9) / .00002 92045 8914D0/
      DATA A( 7,9) / .00000 63763 1144D0/
      DATA A( 8,9) / .00000 14516 7850D0/
      DATA A( 9,9) / .00000 03420 5281D0/
      DATA A(10,9) / .00000 00829 4302D0/
      DATA A(11,9) / .00000 00206 0784D0/
      DATA A(12,9) / .00000 00052 2823D0/
      DATA A(13,9) / .00000 00013 5066D0/
      DATA A(14,9) / .00000 00003 5451D0/
      DATA A(15,9) / .00000 00000 9436D0/
      DATA A(16,9) / .00000 00000 2543D0/
      DATA A(17,9) / .00000 00000 0693D0/
      DATA A(18,9) / .00000 00000 0191D0/
      DATA A(19,9) / .00000 00000 0053D0/
      DATA A(20,9) / .00000 00000 0015D0/
      DATA A(21,9) / .00000 00000 0004D0/
      DATA A(22,9) / .00000 00000 0001D0/

      DATA A( 0,10) / .99343 65167 1347D0/
      DATA A( 1,10) / .02225 77012 6826D0/
      DATA A( 2,10) / .00101 47557 4703D0/
      DATA A( 3,10) / .00008 17515 6250D0/
      DATA A( 4,10) / .00000 89997 3547D0/
      DATA A( 5,10) / .00000 12082 3987D0/
      DATA A( 6,10) / .00000 01861 6913D0/
      DATA A( 7,10) / .00000 00317 4723D0/
      DATA A( 8,10) / .00000 00058 5215D0/
      DATA A( 9,10) / .00000 00011 4739D0/
      DATA A(10,10) / .00000 00002 3652D0/
      DATA A(11,10) / .00000 00000 5082D0/
      DATA A(12,10) / .00000 00000 1131D0/
      DATA A(13,10) / .00000 00000 0259D0/
      DATA A(14,10) / .00000 00000 0061D0/
      DATA A(15,10) / .00000 00000 0015D0/
      DATA A(16,10) / .00000 00000 0004D0/
      DATA A(17,10) / .00000 00000 0001D0/

      IF(N .LT. 1 .OR. N .GT. 4 .OR. P .LT. 1 .OR. P .GT. 4 .OR.
     1   N+P .GT. 5) THEN
       WGPLG=ZERO
       PRINT 1000, N,P
       RETURN
      END IF
      IF(X .EQ. SGN(0)) THEN
       WGPLG=S1(N,P)
       RETURN
      END IF

      IF(X .GT. FCT(2) .OR. X .LT. SGN(1)) THEN
       X1=SGN(0)/X
       H=C1*X1+C2
       ALFA=H+H
       V(0)=SGN(0)
       V(1)=LOG(DCMPLX(-X,ZERO))
       DO 33 L = 2,N+P
   33  V(L)=V(1)*V(L-1)/L
       SK=ZERO
       DO 34 K = 0,P-1
       P1=P-K
       R=X1**P1/(FCT(P1)*FCT(N-1))
       SM=ZERO
       DO 35 M = 0,K
       N1=N+K-M
       L=INDEX(10*N1+P1-10)
       B1=ZERO
       B2=ZERO
       DO 31 I = NC(L),0,-1
       B0=A(I,L)+ALFA*B1-B2
       B2=B1
   31  B1=B0
       Q=(FCT(N1-1)/FCT(K-M))*(B0-H*B2)*R/P1**N1
   35  SM=SM+V(M)*Q
   34  SK=SK+SGN(K)*SM
       SM=ZERO
       DO 36 M = 0,N-1
   36  SM=SM+V(M)*C(N-M,P)
       WGPLG=SGN(N)*SK+SGN(P)*(SM+V(N+P))
       RETURN
      END IF

      IF(X .GT. HALF) THEN
       X1=SGN(0)-X
       H=C1*X1+C2
       ALFA=H+H
       V(0)=SGN(0)
       U(0)=SGN(0)
       V(1)=LOG(DCMPLX(X1,ZERO))
       U(1)=LOG(X)
       DO 23 L = 2,P
   23  V(L)=V(1)*V(L-1)/L
       DO 26 L = 2,N
   26  U(L)=U(1)*U(L-1)/L
       SK=ZERO
       DO 24 K = 0,N-1
       P1=N-K
       R=X1**P1/FCT(P1)
       SM=ZERO
       DO 25 M = 0,P-1
       N1=P-M
       L=INDEX(10*N1+P1-10)
       B1=ZERO
       B2=ZERO
       DO 12 I = NC(L),0,-1
       B0=A(I,L)+ALFA*B1-B2
       B2=B1
   12  B1=B0
       Q=SGN(M)*(B0-H*B2)*R/P1**N1
   25  SM=SM+V(M)*Q
   24  SK=SK+U(K)*(S1(P1,P)-SM)
       WGPLG=SK+SGN(P)*U(N)*V(P)
       RETURN
      END IF

      L=INDEX(10*N+P-10)
      H=C1*X+C2
      ALFA=H+H
      B1=ZERO
      B2=ZERO
      DO 11 I = NC(L),0,-1
      B0=A(I,L)+ALFA*B1-B2
      B2=B1
   11 B1=B0
      WGPLG=(B0-H*B2)*X**P/(FCT(P)*P**N)
      RETURN
 1000 FORMAT(/' ***** CERN SUBROUTINE WGPLG ... ILLEGAL VALUES',
     1        '   N = ',I3,'   P = ',I3)
      END

C-}}}
