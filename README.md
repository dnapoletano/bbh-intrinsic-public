# Public code for bbh-intrinsic FONLL

This code implements and computes the contributions to $`b\bar{b}\to H`$, as presented in [FONLL-intrinsic](http://arxiv.org/abs/arXiv:1905.02207)
and [FONLL-standard](http://arxiv.org/abs/arXiv:1607.00389).

## Installation

```bash
  mkdir build; cd build 
  cmake .. 
  make; make install 
```

## Dependencies
*  [GnuScientificLibrary (gsl)](http://git.savannah.gnu.org/cgit/gsl.git)
*  [LHAPDF](https://lhapdf.hepforge.org)
*  [Yaml-cpp](https://github.com/jbeder/yaml-cpp)

## Usage

```bash
  <installation-path>/bin/bbh_intrinsic
```
Which takes the following options

* ```bash
  -mode : 0(default), 1, 2, 3, 100 
  ```
  Runs the code in one of the 5 pre-defined modes
  * 0  : (default) Computes the FONLL-AP contribution, including all of its ingredients
  * 1  : Computes the FONLL-BP contribution, including all of its ingredients
  * 2  : For a given central $`\mu_R,\mu_F`$ produces the FONLL-AP contribution varying $`\mu_b`$
  * 3  : For a given central $`\mu_R,\mu_F`$ produces the FONLL-BP contribution varying $`\mu_b`$
  * 100: Computes the *matched-b* FONLL-B contributions
  
* ```bash
  -infile : file.yaml
  ```
  Input file containing the paramater set-up, which defaults to [test.yaml](examples/test.yaml). The structure of the input file is the following
  ```yaml
    out_file:             "outfile"             # set the output file-name, no extension
    mb:                   4.92                  # heavy quark pole mass
    mh:                   125.0                 # Higgs mass, or Q^2
    mur:                  33.71                 # Central muR value
    muf:                  33.71                 # Central muF value

    pdfname_var:                                # A list of PDF sets over which repeating the calculation
      - "NNPDF31_nnlo_as_0118"
    member:               0                     # Set the PDF member
    cem:                  13000.0               # Center of mass energy in GeV
    iter:                 10000                 # Number of iterations for the numerical integration routine
    mrf_var:                                    # Scale variation around mur and muf of a given factor. [muR,muF]
      - [0.5, 0.5]
      - [0.5, 1.0]
      - [1.0, 0.5]
      - [1.0, 1.0]
      - [1.0, 2.0]
      - [2.0, 1.0]
      - [2.0, 2.0]
    kmub:                 1.0                   # mub = kmub * mb
  ```
  ### Examples
  To run the example in the [examples](examples) directory, simply install the code
  and
  ```bash
  python3 example.py
  ```
  
## Possible issues
The user may see a crash related to the dependency libraries not being correctly loaded at runtime.
To fix this
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<path-to-the-various-libraries>
```
on Linux, or 
```bash
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:<path-to-the-various-libraries>
```
on mac.

======
# Citations

If you use this code, please remember to cite the following works
```ascii
@article{Forte:2019hjc,
      author         = "Forte, Stefano and Giani, Tommaso and Napoletano, Davide",
      title          = "{Fitting the b-quark PDF as a massive-b scheme: Higgs
                        production in bottom fusion}",
      journal        = "Eur. Phys. J.",
      volume         = "C79",
      year           = "2019",
      number         = "7",
      pages          = "609",
      doi            = "10.1140/epjc/s10052-019-7119-3",
      eprint         = "1905.02207",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "TIF-UNIMI-2019-3",
      SLACcitation   = "%%CITATION = ARXIV:1905.02207;%%"
}
```

```ascii
@article{Forte:2016sja,
      author         = "Forte, Stefano and Napoletano, Davide and Ubiali, Maria",
      title          = "{Higgs production in bottom-quark fusion: matching beyond
                        leading order}",
      journal        = "Phys. Lett.",
      volume         = "B763",
      year           = "2016",
      pages          = "190-196",
      doi            = "10.1016/j.physletb.2016.10.040",
      eprint         = "1607.00389",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "TIF-UNIMI-2016-6, IPPP-16-61, CAVENDISH-HEP-16-11",
      SLACcitation   = "%%CITATION = ARXIV:1607.00389;%%"
}
```

and

```ascii
@article{Forte:2015hba,
      author         = "Forte, Stefano and Napoletano, Davide and Ubiali, Maria",
      title          = "{Higgs production in bottom-quark fusion in a matched
                        scheme}",
      journal        = "Phys. Lett.",
      volume         = "B751",
      year           = "2015",
      pages          = "331-337",
      doi            = "10.1016/j.physletb.2015.10.051",
      eprint         = "1508.01529",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "TIF-UNIMI-2015-12, CAVENDISH-HEP-15-06",
      SLACcitation   = "%%CITATION = ARXIV:1508.01529;%%"
}
```