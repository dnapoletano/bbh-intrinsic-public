#include "bbh-intrinsic-4f.h"
#include "mdefs.h"
// fonll
#include "Massless_lim.H"
// lhapdf
#include "LHAPDF/LHAPDF.h"
// Cmdline
#include "CmdLine.hh"
// gsl
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_vegas.h>
// yaml
#include "yaml-cpp/yaml.h"
// std
#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <cassert>
#include <fstream>

double wrap_calc_func_massive(double z[], size_t dim, void* p)
{
  bbh_intrinsic* instance = (bbh_intrinsic*) p;
  double res = instance->Calc(z, 0);
  return res;
}

double wrap_calc_func_mlim(double z[], size_t dim, void* p)
{
  bbh_intrinsic* instance = (bbh_intrinsic*) p;
  double res = instance->Calc(z, 1);
  return res;
}

double wrap_calc_func_5F(double z[], size_t dim, void* p)
{
  bbh_intrinsic* instance = (bbh_intrinsic*) p;
  double res = instance->Calc(z, 2);
  return res;
}

std::map<std::string, double> 
Integrate_xs(const Scales& sc, const std::string& pdfname, const size_t type, const double mub)
{
  bbh_intrinsic bbh{ sc, pdfname, mub};
  assert(type==0 || type==1 || type==2);
  auto inte{ 0.0 }, err { 0.0 };
  size_t dim{ 3 };
  gsl_monte_function F;
  double xmin[dim], xmax[dim];
  for(size_t i{ 0 }; i < dim; ++i){
    xmin[i] = 0.0;
    xmax[i] = 1.0;
  }
  if(type==0)          F.f = wrap_calc_func_massive;
  else if(type==1)     F.f = wrap_calc_func_mlim;
  else if(type==2)     F.f = wrap_calc_func_5F;
  else exit(1);
  
  F.params=&bbh;

  const auto state{ gsl_monte_vegas_alloc(dim)};
  gsl_monte_vegas_init(state);
  const auto rng{ gsl_rng_alloc(gsl_rng_ranlxs2)};
  gsl_rng_default_seed = 1234;

  gsl_monte_vegas_integrate(&F, xmin, xmax, dim, sc.i_evs, rng, state, &inte, &err);
  do
    {
      gsl_monte_vegas_integrate(&F, xmin, xmax, dim, sc.i_evs, rng, state, &inte, &err);
      printf ("result = % .6f sigma = % .6f "
              "chisq/dof = %.1f\n", inte, err, gsl_monte_vegas_chisq (state));
    }
  while (fabs (gsl_monte_vegas_chisq (state) - 1.0) > 0.5);
  // gsl_monte_vegas_integrate(&F, xmin, xmax, dim, sc.i_evs, rng, state, &inte, &err);

  gsl_rng_free(rng);
  gsl_monte_vegas_free(state);
  std::map<std::string, double> res;
  res["integral"] = inte;
  res["error"]    = err;
  std::cout << " Inte : " << inte << " ± " << err << "\n";
  return res;
}

std::string out_file;

void save_to_scale(const std::string& filename, Scales& sc, std::vector<std::string>& pdfname,
                   std::vector<std::pair<double, double> > &krf, double &mub)
{
  YAML::Node file = YAML::LoadFile(filename);
  std::cout << "  Reading infile, Parameters :\n";  
  for(const auto& t : file){
    std::cout<<"    "<<t.first << ": " << t.second <<std::endl;
    std::string name = t.first.as<std::string>();
    if(name=="out_file")
      out_file    = t.second.as<std::string>();
    if(name=="order"       || name=="order_4F"|| name=="order_5F" )
      sc.i_or     = t.second.as<int>();
    if(name=="order_fonll" || name=="order_FONLL")
      sc.i_fonll  = t.second.as<int>();
    if(name=="mf"          || name=="mb"   || name=="MB")
      sc.mbmb     = t.second.as<double>();
    if(name=="mur"         || name=="mu_R" || name=="MUR")
      sc.mu_R     = t.second.as<double>();
    if(name=="muf"         || name=="mu_F" || name=="MUF")
      sc.mu_F     = t.second.as<double>();
    if(name=="mh"          || name=="mH"   || name=="MH")
      sc.m_H      = t.second.as<double>();
    if(name=="ss"          || name=="cem"  || name=="SS")
      sc.ss       = t.second.as<double>();
    if(name=="iter"        || name=="events")
      sc.i_evs    = t.second.as<int>();
    if(name=="member"      || name=="PDF_member")
      sc.i_member = t.second.as<int>();
    if(name=="a_min"       || name=="a_cut"  || name=="cut")
      sc.m_amin   = t.second.as<double>();
    if(name=="pdfname"     || name=="pdfname_var")
      pdfname     = t.second.as<std::vector<std::string>>();
    if(name=="mrf_var")
      krf         = t.second.as<std::vector<std::pair<double, double> > >();
    if(name=="kmub")
      mub         = t.second.as<double> ();
  }
  std::cout << " --------------------------------\n";
}

struct Args {
  int mode;
};

void default_mode(Scales& sc, const std::vector<std::string>& pdfnames,
		  std::vector<std::pair<double, double> > krf, const double mub,
		  const size_t order=0)
{
  auto outfile = out_file;
  if (order==0){
    outfile = out_file + "_fonll-ap.txt";
  }
  else if (order==1){
    outfile = out_file + "_fonll-bp.txt";
  }
  std::fstream outf{outfile,std::ios::out};
  outf << "pdfname,mub,mur,muf,XS_4F_M_intr,XS_4F_0_intr,XS_5F\n";
  auto mu_R0 = sc.mu_R; auto mu_F0 = sc.mu_F;//auto mub0 = sc.mbmb;
  // pdf variation
  for(auto& pdn: pdfnames){
    auto pdfname_mub = pdn;
    for(auto& k : krf){
      // sc.mbmb = mub0;
      sc.mu_R = mu_R0*k.first; sc.mu_F = mu_F0*k.second;
      std::cout << " ##############################################\n "; 
      std::cout << pdn << " , " << mub << " , " << sc.mu_R << " , " << sc.mu_F << "\n";

      auto four_f_massive  = Integrate_xs(sc, pdfname_mub , 0, mub);
      std::cout << " --------------------------------\n";
      auto four_f_massless = Integrate_xs(sc, pdfname_mub , 1, mub);
      std::cout << " --------------------------------\n";

      std::map<std::string, double> five_f;
      if(order==0)
	five_f          = Integrate_xs(sc, pdfname_mub , 2, mub);
      else if(order==1){
	sc.i_or = 2; sc.i_fonll = -1;
	five_f["integral"] = xsec5F(pdfname_mub.c_str(),sc);
      }
      // auto five_f          = Integrate_xs(sc, pdfname_mub , 2, mub);
      std::cout << " --------------------------------\n";

      outf << pdn << "," << mub << "," << sc.mu_R << "," << sc.mu_F << ","
	   << four_f_massive["integral"]
           << "," << four_f_massless["integral"] << ","
           << five_f["integral"]<< "\n";
    }
  }
  outf.close();

}

void mub_var_mode(Scales& sc, const std::vector<std::string>& pdfnames,
		  std::vector<std::pair<double, double> > krf, const double kmub,
		  const size_t order=0)
{
  //  auto mu_R0 = sc.mu_R; auto mu_F0 = sc.mu_F;
  std::string s(16, '\0');
  auto written = std::snprintf(&s[0], s.size(), "%.2f", kmub);
  s.resize(written);
  auto outfile = out_file + "_mub_" + s;
  if (order==0){
    outfile = outfile + "_fonll-ap.txt";
  }
  else if (order==1){
    outfile = outfile + "_fonll-bp.txt";
  }

  std::fstream outf{outfile,std::ios::out};
  outf << "pdfname,mub,mur,muf,XS_4F_M_intr,XS_4F_0_intr,XS_5F\n";
  for(auto kb: linspace( 10, 0.2, 2.0, true)){
    std::cout << " ##############################################\n "; 
    std::cout << pdfnames[0] << " , " << kb << " , " << sc.mu_R << " , " << sc.mu_F << "\n";

    auto four_f_massive  = Integrate_xs(sc, pdfnames[0] , 0, kb);
    std::cout << " --------------------------------\n";
    auto four_f_massless = Integrate_xs(sc, pdfnames[0] , 1, kb);
    std::cout << " --------------------------------\n";
    std::map<std::string, double> five_f;
    if(order==0)
      five_f          = Integrate_xs(sc, pdfnames[0] , 2, kb);
    else if(order==1){
      sc.i_or = 2; sc.i_fonll = -1;
      five_f["integral"] = xsec5F(pdfnames[0].c_str(),sc);
    }
    std::cout << " --------------------------------\n";

    outf << pdfnames[0] << "," << kb << "," << sc.mu_R << "," << sc.mu_F << ","
	 << four_f_massive["integral"]
	 << "," << four_f_massless["integral"] << ","
	 << five_f["integral"]<< "\n";
    
  }
  outf.close();
}

void old_mode(Scales& sc, const std::vector<std::string>& pdfnames,
	      std::vector<std::pair<double, double> > krf,const double mub)
{
  auto outfile = out_file + "_old_fonll_run.txt";
  std::fstream outf{outfile,std::ios::out};
  outf << "pdfname,mub,mur,muf,XS_4F_0,XS_5F\n";
  auto mu_R0 = sc.mu_R; auto mu_F0 = sc.mu_F;//auto mub0 = sc.mbmb;
  // pdf variation
  for(auto& pdn: pdfnames){
    auto pdfname_mub = pdn;
    for(auto& k : krf){
      sc.mu_R = mu_R0*k.first; sc.mu_F = mu_F0*k.second;
      std::cout << " ##############################################\n "; 
      std::cout << pdn << " , " << mub << " , " << sc.mu_R << " , " << sc.mu_F << "\n";
      std::map<std::string, double> five_f, mlim;
      sc.i_or = -1; sc.i_fonll = 2;
      mlim["integral"]   = xsec4F0(pdfname_mub.c_str(),sc);
      std::cout << " --------------------------------\n";
      sc.i_or = 2; sc.i_fonll = -1;
      five_f["integral"] = xsec5F(pdfname_mub.c_str(),sc);
      std::cout << " --------------------------------\n";

      outf << pdn << "," << mub << "," << sc.mu_R << "," << sc.mu_F << ","
	   << mlim["integral"]  << ","
           << five_f["integral"]<< "\n";
    }
  }
  outf.close();

}


int main(int argc, char* argv[]){
  Args args;
  CmdLine cmd = CmdLine(argc, argv);
  Scales sc{ 2, 2, 4.92, 125., 125., 125., 13000., 10000, 0, 1.e-12 };
  std::vector<std::string> pdfnames;
  std::vector<std::pair<double, double> > krf;
  double mub;
  krf.clear(); pdfnames.clear();
  std::string infile = "test.yaml";
  if(cmd.present("-infile"))
    infile = cmd.value<std::string>("-infile");
  PRINT_VAR(infile);
  save_to_scale(infile, sc, pdfnames, krf, mub);

  // Now check if the command line overides some values
  // and set running mode
  /* -------------------------- Running modes ------------------------------
     running mode: 0   = default, mur/f variation for a given mu0 and mub
                         for all pdf sets provided in the yaml file

     running mode: 1   = same as 0 but fonll-bp

     running mode: 2   = mub-var, mub variation for a given mur-muf and mub0
                         for all pdf sets

     running mode: 3   = same as 2 but fonll-bp

     running mode: 100 = computes ingredients for old fonll-b
  */
  args.mode = cmd.value("-mode", 0);
  if(cmd.present("-kmub"))
    mub = cmd.value("-kmub",1.0);  

  if(args.mode==0) // default
    default_mode(sc, pdfnames,krf, mub);

  if(args.mode==1) // default
    default_mode(sc, pdfnames,krf, mub,1);

  if(args.mode==2) // mub-var
    mub_var_mode(sc, pdfnames,krf, mub);
  
  if(args.mode==3) // mub-var
    mub_var_mode(sc, pdfnames,krf, mub,1);

  if(args.mode==100) // default
    old_mode(sc, pdfnames,krf, mub);
  
  return 0;
}
