#ifndef BBH_INTRINSIC_4F__H
#define BBH_INTRINSIC_4F__H

#include "Massless_lim.H"
#include "PDF.H"

#include "LHAPDF/LHAPDF.h"

#include <map>
#include <string>
#include <complex>

#include "gsl/gsl_sf_dilog.h"

#include "mdefs.h"
#include "extern.h"

class bbh_intrinsic{
 private:
  Massless_Limit * p_mlim;
  LUMI * p_lumi;
  double m_m2h, m_m2b, m_S, m_tauh, m_mu2b;
  double m_beta2, m_beta, m_m2r, m_m2f;
  double m_sigma0,m_sigma0_mlim, m_cf, m_tr;
 public:
  bbh_intrinsic(const Scales& sc, const std::string& pdfname, const double mub);
  ~bbh_intrinsic();
  // the LO contribution is just delta(1-y)
  // The following objects are all distributions,
  // four entries are set != 0: "delta", "regular", "plus" and "reg_plus"
  // where the last one is the regular part of the plus distribution
  dist delta1_bb(const double y);
  dist delta1_bg(const double y);
  // Massless-limit
  dist delta1_bb_0(const double y);
  dist delta1_bg_0(const double y);
  
  dist D(const double y, const size_t n);

  inline double sqr(double x) {return x*x;}
  inline double lbb(const double x, const double y)
  {return 2.*p_lumi->Lumi(10, 0, x, m_tauh/x/y, 1., 1., 10);}
  inline double lbg(const double x, const double y) 
  {return 2.*p_lumi->Lumi(10, 5, x, m_tauh/x/y, 1., 1., 10)
      + 2.*p_lumi->Lumi(5, 10, x, m_tauh/x/y, 1., 1., 10);}
  double Calc(double z[], const size_t type);
};
#endif
