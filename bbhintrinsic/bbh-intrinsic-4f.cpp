#include "bbh-intrinsic-4f.h"
// fonll-code
#include "Massless_Kernels.H"
#include "Massless_lim.H"
#include "PDF.H"
// LHAPDF
#include "LHAPDF/LHAPDF.h"
// std
#include <iostream>
#include <cmath>
#include <complex>

// TODO: Modify so that one can choose if change of scheme or not
using dist = std::map<std::string,double>;

bbh_intrinsic::bbh_intrinsic(const Scales& sc, const std::string& pdfname, const double mub):
  p_mlim{ NULL },
  p_lumi{ new LUMI(pdfname.c_str(),sc.i_member,
		   sc.mu_F,sc.mbmb,false) }
{
  m_m2h    = sqr(sc.m_H); m_m2b = sqr(sc.mbmb);
  m_S      = sc.ss; m_tauh = m_m2h/sqr(m_S);
  m_mu2b   = sqr(mub)*m_m2b;
  m_beta2  = 1.-4.*m_m2b/m_m2h;
  m_beta   = sqrt(m_beta2);
  m_m2r    = sqr(sc.mu_R); m_m2f = sqr(sc.mu_F);

  const auto mbr   = p_lumi->mb(4.187,sqrt(m_m2r),4);
  std::cout << mbr << std::endl;
  double cv   = 3.8937966e8;
  double vev  = 246.218;
  
  m_sigma0        = M_PI/6. * cv/m_m2h * sqr(mbr/vev) * m_beta;
  m_sigma0_mlim   = M_PI/6. * cv/m_m2h * sqr(mbr/vev);
  m_cf     = 4./3.;
  m_tr     = 1./2.;

  std::cout << "  Init Ok ! " << std::endl;

}

bbh_intrinsic::~bbh_intrinsic()
{
  if(p_mlim)
    delete p_mlim;
  if(p_lumi)
    delete p_lumi;
}

// change of scheme already done.
dist bbh_intrinsic::delta1_bb(const double x)
{
  assert(x<=1.);
  dist res;
  double beta2 = 1.-4.*m_m2b/m_m2h*x;
  double beta  = sqrt(beta2);
  double lrf   = log(m_m2r/m_m2f);

  double ga0   = (1.+m_beta2)/(2.*m_beta);
  double d0    = (1.+m_beta)/(1.-m_beta);
  double ld0   = log(d0);

  double ga    = (1.+beta2)/(2.*beta);
  double d     = (1.+beta)/(1.-beta);
  double ld    = log(d);
  //  double ldga  = ga*ld; 

  double xi    = 1. + log((1.-m_beta2)/sqr(1.+m_beta2))
    + (5.-7*m_beta2)*ld0/4./m_beta
    + (1.+m_beta2)/m_beta*( sqr(M_PI)/6. + 2.*dilog(1./d0)
			    -ld0*log(4.*m_beta2/(1+m_beta2)/(1+m_beta)));

  res["delta"]    = xi - 2 + 3./2.*(ga0*log(sqr(1.+m_beta)/4.)+
				    ga0*log(m_m2h/m_m2b) + lrf );

  res["regular"]  =  -(2. + x + sqr(x))*( ga*log(m_m2h/m_m2b)+ log(m_mu2b/m_m2f)+
					  ga*log(sqr(1.+beta)/4.) - ga*log(x)+
					  2.*log(1.-x) )
    + x*(1.-x) - ld*x*((1.-beta2)/beta2)/m_beta/(1.-2.*x-beta2)
    -2.*ga*log(x)/(1-x);
  res["plus_0"]   = 2.*(ga*log(sqr(1+beta)/4.) + ga*log(m_m2h/m_m2b) + log(m_mu2b/m_m2f));
  res["plus_1"]   = 4.0;
  return res*m_cf/M_PI;
}

dist bbh_intrinsic::delta1_bg(const double x)
{
  // checked!
  assert(x<=1.);
  dist res;
  double beta2 = 1.-4.*m_m2b/m_m2h*x;
  //  double beta  = sqrt(beta2);
  double pqg   = 1 - 2.*x + 2.*sqr(x);
  double lam2  = sqr(3.+beta2) + 16.*sqr(x)-8.*x*(5-beta2);
  if(x>1./((5.-m_beta2)/4.+sqrt(1.-m_beta2))){
    lam2 = 0.0;
  }
  double lam   = sqrt(lam2);
  //  double beta0 = m_beta;
  res["delta"]          = 0.0;
  res["regular"]        =
    x*(4096*(1.-beta2)*(beta2+x-1)/(sqr(beta2+4*x-5)-lam2)*lam
    	   -64.*(9.*sqr(beta2)+(40*x-42)*beta2+8.*x*(4*x-9)+49.)*
    	   ArcTanh(lam/(beta2+4*x-5))
    	   + (5-beta2)*(sqr(beta2)+beta2*(4*x+22.)+44.*x-71)*lam)
    /16./m_beta/pow(3.+beta2,3)
    -x*pqg*log(m_m2f/m_mu2b)/2.;
  res["plus_0"]         = 0.0;
  res["plus_1"]         = 0.0;
  return res*m_tr/M_PI;
}

// Massless limit function
dist bbh_intrinsic::delta1_bb_0(const double x)
{
  assert(x<=1.);
  dist res;
  double lrf = log(m_m2r/m_m2f);
  double lhf = log(m_m2h/m_m2f);
  double lkb = log(m_mu2b/m_m2b);
  
  res["delta"]          = -1. + sqr(M_PI)/3. + 3./2.*lrf;
  res["regular"]        = 
    -2.*log(x)/(1-x) - (2.+x+sqr(x))*(lhf + lkb + log(sqr(1.-x)/x)) + x*(1.-x);
  res["plus_0"]         = 2.*(lhf + lkb);
  res["plus_1"]         = 4.0;
 
  return res*m_cf/M_PI;
}

dist bbh_intrinsic::delta1_bg_0(const double x)
{
  assert(x<=1.);
  dist res;
  double lhf = log(m_m2h/m_m2f);
  double lkb = log(m_mu2b/m_m2b);
  double pqg   = 1 - 2.*x + 2.*sqr(x);
  
  res["delta"]          = 0.0;
  res["regular"]        = x*pqg/2.*(log(sqr(1-x)/x) + lhf + lkb) - x*(1.-x)*(3-7.*x)/4;
  res["plus_0"]         = 0.0;
  res["plus_1"]         = 0.0;
  return res*m_tr/M_PI;
}

dist bbh_intrinsic::D(const double y, const size_t n)
{
  dist res;
  res["plus"] = pow(log(1.-y),n)/(1.-y);
  res["inte"] = -1./(n+1.) * pow(log(1.-y),n+1);
  return res;
}

double bbh_intrinsic::Calc(double z[], const size_t type)
{
  auto tau   = m_tauh*pow(1./m_tauh,z[0]);
  auto jact  = tau*log(1./m_tauh);
  auto jac = jact;

  auto ximax = 1./2.*log(1./tau);
  auto xi    = -ximax + 2.*ximax*z[1];

  jac       *= 2.*ximax;

  // variables for double integration
  // for delta, just use tau
  auto x = sqrt(tau)*exp(xi);
  auto y = sqrt(tau)*exp(-xi);

  //
  double s0 = (type==0)?m_sigma0:m_sigma0_mlim;
  
  auto alpha{ p_lumi->ass(m_m2r)*2.*M_PI };
  // lo : is just a delta function
  auto leading_order = jact*lbb(tau, 1.)/tau;

  // nlo: c-1 delta(1-x) + c0 plus(1-x,0) + c1 plus(1-x,1) + reg - end_point of plus
  auto dbb_1 = [this,type](double x){
    if(type==0) return this->delta1_bb(x);
    else if(type==1) return this->delta1_bb_0(x);
    else if(type==2) {
      dist temp;
      double lfr = log(m_m2f/m_m2r);
      double lfh = log(m_m2f/m_m2h);
      temp["delta"]   = delta1_(&lfr);
      temp["regular"] = sbbq1_(&x, &lfh);
      temp["plus_0"]  = -8./3.*lfh ; 
      temp["plus_1"]  = 16./3.;
      return temp/M_PI;
    }
    else return this->delta1_bb(x);
  };
  auto dbg_1 = [this,type](double x){
    if(type==0) return this->delta1_bg(x);
    else if(type==1) return this->delta1_bg_0(x);
    else if(type==2) {
      dist temp;
      double lfh = log(m_m2f/m_m2h);
      temp["delta"]   = 0.0;
      temp["regular"] = sbg1_(&x, &lfh);
      temp["plus_0"]  = 0.0;
      temp["plus_1"]  = 0.0;
      return temp/M_PI;
    }
    else return this->delta1_bg(x);
  };

  auto delta_1 = jact*(lbb(tau, 1.)*(dbb_1(1.)["delta"] -
                                     dbb_1(1.)["plus_0"]*D(m_tauh/tau,0)["inte"]-
                                     dbb_1(1.)["plus_1"]*D(m_tauh/tau,1)["inte"]))/tau;

  auto c0_1    = jac*D(y,0)["plus"]*(lbb(x, y)*dbb_1(y)["plus_0"] -
                                     y*(lbb(x, 1.)*dbb_1(1.)["plus_0"]) )/tau;

  auto c1_1    = jac*D(y,1)["plus"]*(lbb(x, y)*dbb_1(y)["plus_1"] -
                                     y*(lbb(x, 1.)*dbb_1(1.)["plus_1"]) )/tau; 

  auto r_1     = jac*(lbb(x, y)*dbb_1(y)["regular"] + lbg(x, y)*dbg_1(y)["regular"])/tau;

  auto nlo = (delta_1 + c0_1 + c1_1 + r_1);
  return s0*(leading_order + alpha*(nlo));
}
