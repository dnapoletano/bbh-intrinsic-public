#ifndef MDEFS_H
#define MDEFS_H
/*************************************************************************

                        Mathematica source file

        Copyright 1986 through 1999 by Wolfram Research Inc.


*************************************************************************/

/* C language definitions for use with Mathematica output */
#include <map>
#include <vector>

#define Power(x, y)	(pow((double)(x), (double)(y)))
#define Sqrt(x)		(sqrt((double)(x)))

#define Abs(x)		(fabs((double)(x)))

#define Exp(x)		(exp((double)(x)))
#define Log(x)		(log((double)(x)))

#define Sin(x)		(sin((double)(x)))
#define Cos(x)		(cos((double)(x)))
#define Tan(x)		(tan((double)(x)))

#define ArcSin(x)       (asin((double)(x)))
#define ArcCos(x)       (acos((double)(x)))
#define ArcTan(x)       (atan((double)(x)))

#define Sinh(x)         (sinh((double)(x)))
#define Cosh(x)         (cosh((double)(x)))
#define Tanh(x)         (tanh((double)(x)))

#define PolyLog(i,x)    gsl_sf_dilog((double)x)
#define ArcTanh(x)      0.5*log((1+(double)x)/(1-(double)x))

#define E		2.71828182845904523536029
#define Pi		3.14159265358979323846264
#define Degree		0.01745329251994329576924


/** Could add definitions for Random(), SeedRandom(), etc. **/


#define RED   "\u001b[31m"
#define BLUE  "\u001b[34m"
#define RESET "\u001b[0m"

#define PRINT_VAR(x) std::cout << RED << #x << " :\n" << RESET<< x << "\n"

using dist = std::map<std::string,double>;
  
inline dist operator*(const dist& d, const double& b)
{
  dist res;
  for(auto& di: d)
    res[di.first] = di.second*b;
  return res;
}
  
inline dist operator*(const double& b,const dist& d)
{
  return d*b;
}
  
inline dist operator/(const dist& d, const double& b)
{
  return d*(1./b);
}

inline dist operator+(dist& a,dist& b)
{
  dist res;
  for(auto& di: a)
    res[di.first] = a[di.first] + b[di.first];
  return res;
}

inline std::ostream& operator<<(std::ostream& os, const dist& dt)
{
  for(auto& d: dt)
    os<< "  " << d.first << " = " << d.second << "\n";
  return os;
}

inline std::vector<double> linspace(size_t nbins, double start,
				    double end, bool include_end=true) {
  assert(end >= start);
  assert(nbins > 0);
  std::vector<double> rtn;
  const double interval = (end-start)/static_cast<double>(nbins);
  for (size_t i = 0; i < nbins; ++i) {
    rtn.push_back(start + i*interval);
  }
  assert(rtn.size() == nbins);
  if (include_end) rtn.push_back(end); // exact end, not result of n * interval
  return rtn;
}

#endif
